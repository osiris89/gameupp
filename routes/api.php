<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/games', function(){
    $games = App\Game::all();
    foreach($games as $game)
    {
        $game['field'] = $game->field;
    }

    return $games;
});

Route::get('/fields', function(){
    $fields = App\Location::all();
    return $fields;
});

Route::get('/new_field', function(Request $request){
    $field_name = $request->get('name');
    $field_address = $request->get('address');
    $field_zipcode = $request->get('zipcode');
    $field_lat = $request->get('lat');
    $field_lng = $request->get('lng');
    $new_field = new App\Location;
    $new_field->name = $field_name;
    $new_field->address = $field_address;
    $new_field->zipcode = $field_zipcode;
    $new_field->lat = $field_lat;
    $new_field->lng = $field_lng;
    $new_field->state = 'IL';
    $new_field->city = 'Chicago';
    $new_field->neighborhood = $request->get('area');
    $new_field->save();
    print_f($request->all());
});