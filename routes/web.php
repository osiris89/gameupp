<?php
use App\Events\MessagePosted;
use App\Game;
use Illuminate\Http\Request;
/*
 * Auth Routes
 */
Auth::routes();

/*
 * NeighborhoodController (Resource Controller)
 */
Route::resource('hood', 'NeighborhoodController');

/*
 * HomeController
 */
Route::get('/',  'HomeController@index')->name('games');

/*
 * UserController
 */
Route::get('/home', 'UserController@userProfile')->middleware('auth')->name('home');
Route::get('/user/{id}', 'UserController@publicProfile');
Route::post('/info', 'UserController@addUserInfo');
Route::any('add_user_info', 'UserController@addUserInfo');


/*
 * GameController
 */
Route::resource('games', 'GameController');
Route::post('/create_game', 'GameController@create');
Route::get('/create_game', 'GameController@newGameForm')->middleware('auth');
Route::post('/updateGame', 'GameController@update');
Route::get('/design', 'GameController@showNeighborhoods');

/*
 * ChicagoDataScrapperController
 */
Route::get('/chidata', 'ChicagoDataScraperController@getNeighborhoods');
Route::get('/chitest', 'ChicagoDataScraperController@getNeighborhoodPictures');
Route::get('/imgs', 'ChicagoDataScraperController@imgToHood');
Route::get('/parks', 'ChicagoDataScraperController@getChicagoParks');

/*
 * FieldsController
 */
Route::post('/vote', 'FieldsController@vote');

/*
 * PostController
 */
Route::any('upload_picture', 'PostsController@photoUpload')->name('pic_upload');

/*
 * Views
 */
Route::view('/login', 'auth.login')->name('login');
Route::view('/about', 'about');
Route::view('/user_info', 'auth/user_info_register');
Route::view('/chatroom', 'chatroom')->middleware('auth');

Route::match(['post', 'get'], '/delete_photo', function(Illuminate\Http\Request $request){
    $photo_id = explode('user-image-', $request->get('photo_id'))[1];
    $photo = App\UserPhoto::find($photo_id);
    $photo->delete();
});

Route::get('/messages', function(){
    return  App\Message::with('user')->get();
})->middleware('auth');

Route::post('/messages', function(){
    $user = Auth::user();
    $message = $user->message()->create([
        'message' => request()->get('message')
    ]);
    //Announce new message has been posted
    broadcast(new MessagePosted($message, $user))->toOthers();
    return ['status' => 'success'];
})->middleware('auth');

//Route::get('/games/{id}', function($id){
//    $game = App\Game::find($id);
//    return view('main.area_games', compact('game'));
//});

Route::get('/email', 'EmailController@testEmail');


Route::view('/sp', 'main.choose_sport');
Route::view('/friends', 'main.user_friends');
