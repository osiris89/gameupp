<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Game;
use App\Sports;
use App\Http\Controllers\LocationsController;
use App\Http\Controllers\UserController;
use Faker\Generator as Faker;

$number_of_fields = LocationsController::numberOfFields();
$number_of_users = UserController::numberOfUsers();
$number_of_sports = count(Sports::all());

$factory->define(Game::class, function (Faker $faker) use($number_of_fields, $number_of_users, $number_of_sports) {
    $rand_user = rand(1, $number_of_users);
    $participating_users = getParticipatingUsers($rand_user);
    $sport_name = Sports::find(rand(1, $number_of_sports))->name;
    return [
        'field_id' => rand(1, $number_of_fields),
        'user_id' => $rand_user,
        'sport' => $sport_name,
        'players' => $participating_users,
        'number_of_players' => numberOfPlayers($rand_user),
        'votes' => count(explode(',', $participating_users)),
        'play_time' => $faker->time(),
        'play_date' => $faker->dateTimeBetween('now', '1 month')
    ];
});


function getParticipatingUsers($number_of_users)
{
    $rand_user = rand(1, $number_of_users); //4
    $participating = array($rand_user);
    for($i = 0; $i < $rand_user; $i++)
    {
        $new_participant = rand(1, $rand_user);
        if($new_participant != $rand_user && !in_array($new_participant, $participating))
        {
            $participating[] = $new_participant;
        }
    }
    return implode(',', $participating);
}

function numberOfPlayers($rand_user)
{
    return $rand_user != 1 ? $rand_user : 2;
}

/*
 *  $table->integer('field_id')->unsigned();
    $table->integer('user_id')->unsigned();
    $table->string('sport', 20);
    $table->string('players');
    $table->smallInteger('number_of_players')->unsigned();
    $table->smallInteger('votes')->unsigned();
    $table->time('play_time');
    $table->date('play_date');
 */




