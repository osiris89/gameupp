<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->nullableTimestamps();
        });

        DB::table('sports')->insert(
            array(
                [ 'name' => 'soccer' ],
                [ 'name' => 'tennis'],
                [ 'name' => 'basketball'],
                [ 'name' => 'volleyball'],
                [ 'name' => 'football'],
                [ 'name' => 'ultimate'],
                [ 'name' => 'baseball'],
                [ 'name' => 'rugby'],
                [ 'name' => 'table tennis'],
                [ 'name' => 'pool']
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sports');
    }
}
