<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('location_id');
            $table->string('supervisor');
            $table->string('phone_number');
            $table->string('features');
            $table->integer('park_hours')->nullable();
            $table->integer('fieldhouse_hours')->nullable();
            $table->text('description');
            $table->string('web_link');
            $table->timestamps();
        });

        Schema::table('parks', function($table) {
            $table->foreign('location_id')
                ->references('id')
                ->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parks');
    }
}
