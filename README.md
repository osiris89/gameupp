A Laravel mobile web application that connects people with a shared passion for sports.
Gameupp allows users to create 'pickup' games in and around the Chicago land area.
From tennis, to soccer and many other sports, users can find people from around their 
neighborhoods to connect and play their favorite sports.