<?php

namespace App\Listeners;

use App\Events\NewGameCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Location;

class CreateNewField
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
////////////////////////////// TODO Finish passing the name and the lat/lng values of the location ////////////////////
    /**
     * Handle the event.
     *
     * @param  NewGameCreated  $event
     * @return void
     */
    public function handle(NewGameCreated $event)
    {
        // print_f($event->field_data);exit;
        $field = $event->field_data;
        $new_location = new Location();
        $new_location->name = $field['field_name'];
        $new_location->type = $field['type'];
        $new_location->zipcode = $field['postal_code'];
        $new_location->city = $field['city'];
        $new_location->state = $field['state'];
        $new_location->neighborhood = $field['neighborhood'];
        $new_location->address = $field['street_number'] . ' ' . $field['street_name'];
        $new_location->lat = $field['lat'];
        $new_location->lng = $field['lng'];
        $new_location->save();
        return $new_location;
    }
}
