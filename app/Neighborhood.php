<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Neighborhood extends Model
{
    var $guarded = [];

    public function field()
    {
        return $this->hasMany('App\Location', 'neighborhood', 'name');
    }

//    public function games()
//    {
//        return $this->hasMany('App\Game');
//    }
}
