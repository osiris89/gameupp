<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\UserPhoto;


class PostsController extends Controller
{
    /*
     * Takes the user selected picture from the footer button and uploads the picture to the 'user_images' folder in the public directory.
     */
    public function photoUpload(Request $request)
    {
        $image = $request->file('image-upload');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $location = public_path('user_images/' . $filename);
        $profile_picture = $request->get('profile_picture') == 'on' ? true : false;
        Image::make($image)->resize(600, 600)->save($location);
        $this->photoDatabaseUpload($filename, $profile_picture);
        return redirect()->route('home')->with('success', "Image Uploaded!");
    }


    /**
     * Saves the location of the uploaded image to the database.
     * @param string $filename - The file name of the uploaded picture. Used to save to the database.
     * @param boolean $profile_picture - If true, the uploaded picture should be saved as the user's profile picture.
     */
    public function photoDatabaseUpload($filename, $profile_picture)
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        if($profile_picture)
        {
            DB::table('profiles')->where('user_id', '=', $user_id)->update(['profile_picture' => $filename]);
        }

        $user_photo = new UserPhoto;
        $user_photo->user_id = $user_id;
        $user_photo->file_path= $filename;
        $user_photo->save();
    }


}
