<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Park;
use App\LocationHour;

class ParksController extends Controller
{
        public function create($location_id, $park_data, $location_hour_ids)
        {
            $new_park = Park::where('location_id', '=', $location_id)->first() ?: new Park;
            $new_park->location_id      = $location_id;
            $new_park->supervisor       = $park_data['supervisor'];
            $new_park->phone_number     = $park_data['phone_number'];
            $new_park->features         = $park_data['features'];
            $new_park->park_hours       = isset($location_hour_ids['park_times']) ?: null;
            $new_park->fieldhouse_hours = isset($location_hour_ids['fieldhouse_times']) ?: null;
            $new_park->description      = $park_data['description'];
            $new_park->web_link         = $park_data['web_link'];
            $new_park->save();
        }
}
