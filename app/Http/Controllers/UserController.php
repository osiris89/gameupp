<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;

class UserController extends Controller
{

    public static function numberOfUsers()
    {
        return count(User::all());
    }

    /*
     *
     */
    public function index()
    {

    }

    /*
     *
     */
    public function userProfile()
    {
        $user_id = Auth::User()->id;
        $user = User::find($user_id);
        $user_name = $user->name;
        $users_photos = $user->userPhotos;
        $user_info = $user->profile;
        $profile_image = $user->profile->profile_picture;
        return view('main.user_profile', [
            'user_name' => $user_name,
            'user_info' => $user_info,
            'profile_picture' => $profile_image,
            'photos' => $users_photos
        ]);
    }

    /*
     * New function
     */
    public function addUserInfo(Request $request)
    {
        $user_id = Auth::user()->id;
//        print_f($request->all());
        $sports = implode(',', $request->get('sports'));
        $zipcode = $request->zipcode;
        $comp_level = $request->competitive_level;
        $user_info = Profile::where('user_id', $user_id)->first();
        $user_info->sports = $sports;
        $user_info->zipcode = $zipcode;
        $user_info->competitive_level = $comp_level;
        $user_info->save();
        return redirect()->route('home');
    }

    public function publicProfile($id)
    {
        $user = User::find($id)->load('profile');
        return view('users.public_profile', ['user' => $user]);
    }

    /*
     * Return the current set profile picture of the user
     */
    public function usersProfilePicture($id)
    {

    }

}
