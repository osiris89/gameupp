<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Events\NewGameCreated;
use App\Game;
use App\Sports;
use App\User;
use App\Location;
use App\Profile;
use Carbon\Carbon;


class GameController extends Controller
{
    /*
        TODO Finish implementing this function. You have to split the array of games into two arrays. One with games of the same field, the other with unique fields. 
    */
    public function sortGames()
    {
        $all_games = Game::all();
        $todaysGames = array();
        $upcomingGames = array();
        foreach($all_games as $index => $game)
        {
            if($game->play_date == date("Y-m-d"))
            {
                $this->addInfoToGame($game);
                $todaysGames[] = $game;
            }
            else
            {
                $upcomingGames[] = $game;
                $this->addInfoToGame($game);
            }

        }
        return compact('todaysGames', 'upcomingGames');
    }

    public function addInfoToGame(&$game)
    {
        $play_time = date( 'g:i A', strtotime($game->play_time));
        $game['play_time']     = $play_time;
        $game['field']         = $game->field;
        $game['attending']     = $this->getPlayersAttending($game);
    }

    private function addMoreData(&$field_data, $request)
    {
        $field_data["type"]       = $request->get('type');
        $field_data["field_name"] = $request->get('field_id');
        $field_data["lat"]        = $request->get('lat');
        $field_data["lng"]        = $request->get('lng');
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $new_game = new Game;
        if(array_key_exists('location', $request->all())) // If 'location' key exists, it indicates that there is a new field selected and should be created.
        {
            $this->createField($request, $new_game);
        }
        else
        {
            $new_game->field_id = $request->field_id;
        }
        $new_game->user_id = Auth::id();
        $new_game->sport = $request->sport;
        $new_game->players = Auth::id();
        $new_game->number_of_players = $request->number_of_players;
        $new_game->votes = 1;
        $new_game->play_time = $request->play_time;
        $new_game->play_date = $request->play_date;
        $new_game->save();
        return redirect()->route('games');
    }

    public function createField($request, &$new_game)
    {
        $field_data = $request->get('location');
        $this->addMoreData($field_data, $request);
        $new_location = event(new NewGameCreated($field_data));
        $new_game->field_id = $new_location[0]['id'];
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $game = Game::with('field.neighborhood')->find($id);
        $players = $this->getPlayersAttending($game);
        $voted = $this->alreadyVoted(Auth::user()->id, explode(',', $game->players));
        return !Auth::check() ? redirect('/') : view('main.game_details', compact('voted', 'game', 'players'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
//        print_f($request->all());
        $user_id   = $request->get('user_id');
        $game      = Game::find($request->get('game_id'));
        $vote_type = $request->get('vote_type');
        if ($this->alreadyVoted($user_id, $game->players) && $vote_type == 'up_vote')
        {
            return Response::create('Already Voted!', 422);
        }
        $game->votes = $vote_type == 'up_vote' ? ++$game->votes : --$game->votes;
        $game->players = $this->editPlayer($user_id, $game->players, $vote_type);
        $game->save();
        return json_encode($game);
    }

    public function alreadyVoted($user_id, $players)
    {
        if(is_array($players))
            return in_array($user_id, $players);
        else
            return in_array($user_id, explode(',', $players));
    }

    public function editPlayer($user_id, $players, $vote_type)
    {
        if($vote_type == 'up_vote')
        {
            $players .= ','.$user_id;
            return $players;
        }
        else
        {
            $pplayers = explode(',', $players);
            unset($pplayers[array_search($user_id, $pplayers)]);
            $pplayers = implode(',', $pplayers);
            return $pplayers;
        }
        return false;
    }



    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function newGameForm()
    {
        $user_id = Auth::id();
        $user = User::find($user_id);
        $sports = explode(',', $user->profile->sports);
        $other_sports = [];
        Sports::all('name')->each(function($item, $key) use($sports, &$other_sports){
            if(!in_array($item->name, $sports))
            {
                $other_sports[] = $item->name;
            }
        });
        $locations = Location::all();
        return view('main.create_game', ['sports' => $sports, 'other_sports' => $other_sports, 'locations' => $locations]);
    }

    /*
     * Displays the 'layouts.view_games.blade' file. Retrieves all of the games and their respective neighborhood image.
     * TODO: Retrieve only upcoming games, instead of all games.
     */
    public function getNeighborhoods()
    {
        $games = Game::with('field.neighborhood')->get();
        $hoods = array();
        $games->each(function($game) use(&$hoods){
            $hood = $game['field'];
            $hood_name = $game['neighborhood'];
            $hood_id = $game['id'];
            if(!array_key_exists($hood_name, $hoods))
            {
                $hoods[$hood_name]['id'] = $hood_id;
                $hoods[$hood_name]['count'] = 1;
                $hoods[$hood_name]['url'] = $hood['img_url'];
            }
            else
            {
                $hoods[$hood_name]['count'] += 1;
            }
        });
        return $hoods;
    }

    public function getPlayersAttending($game)
    {
        $players = explode(',', $game->players);
        return User::with('profile')->findMany(array_values($players));
    }

}






















