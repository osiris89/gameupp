<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Location;
use App\LocationHour;
use App\Http\Controllers\ParksController;

class LocationsController extends Controller
{
    protected $parkController;

    public function __construct(ParksController $parkController)
    {
        $this->parkController = $parkController;
    }

    public static function numberOfFields()
    {
        return count(Location::all());
    }

    public function getFields()
    {
        $fields = Location::all();
        $images = array();
        foreach($fields as $field)
        {
            $user_ids = explode(',', $field->players);
            foreach($user_ids as $user_id)
            {
                $user = User::find($user_id);
                $default_image = $user->defaultImage();
                $field->player_images = array($default_image);
            }
            $images[$field->name] = $field->images;
        }
        $fields_and_images = array('fields' => $fields, 'images' => $images);
        return $fields_and_images;
    }
    
    public function vote(Request $request)
    {

    }

    public function create($location_data)
    {
        $park_name = $location_data['name'];
        if(Location::where('name', '=', $park_name)->first() == null) {
            $location_data['coordinates'] = $this->getLatAndLng(urlencode($location_data['address']));
            $new_field = Location::where('name', '=', $park_name)->first() ?: new Location;
            $new_field->name = $park_name;
            $new_field->type = 'Park';
            $new_field->address = $location_data['address'];
            $new_field->city = 'Chicago';
            $new_field->state = 'IL';
            $new_field->zipcode = $location_data['zipcode'];
            $new_field->neighborhood = $location_data['coordinates']['neighborhood'];
            $new_field->lat = $location_data['coordinates']['lat'];
            $new_field->lng = $location_data['coordinates']['lng'];
            $new_field->save();

            $new_location_id = $new_field->id;
            $location_hour_ids = $this->addLocationHours($new_location_id, $location_data);
            $this->parkController->create($new_location_id, $location_data, $location_hour_ids);
        }
    }

    private function addLocationHours($location_id, $location_data)
    {
        $location_hour_ids = [];
        foreach($location_data['hours'] as $type => $hours)
        {
            $location_hours = new LocationHour;
            $location_hours->location_id = $location_id;
            $location_hours->type = $type;
            foreach($hours as $day => $hour)
            {
                $location_hours->$day = $hour;
            }
            $location_hours->save();
            $location_hour_ids[$type] = $location_hours->id;
            unset($location_hours);
        }
        return $location_hour_ids;
    }

    private function getLatAndLng($address)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/geocode/json?address=$address+Chicago+IL&key=AIzaSyCDa6QBm2_sAunMD27rznV8y59LDSF1OUc");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $results = json_decode($output, true);
        $neighborhood = $results['results'][0]['address_components'][2]['long_name'];
        $lat = $results['results'][0]['geometry']['location']['lat'];
        $lng = $results['results'][0]['geometry']['location']['lng'];
        curl_close($ch);
        return ['lat' => $lat, 'lng' => $lng, 'neighborhood' => $neighborhood];
    }
}
