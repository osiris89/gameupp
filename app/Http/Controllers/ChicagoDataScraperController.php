<?php

namespace App\Http\Controllers;
use Goutte;
use App\Neighborhood;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class ChicagoDataScraperController extends Controller
{
    /*
     * This function scrapes (http://www.city-data.com/indexes/neighborhoods/IL/) for stock pics of Chicago neighborhoods
     */
    public function getNeighborhoods()
    {
        $page = 4;
        $crawler = Goutte::request('GET', 'http://www.city-data.com/indexes/neighborhoods/IL/'.$page.'/');
        sleep(4);
        $crawler->filter('.index li a')->each(function ($node) {
            $neighborhood_link = $node->extract('href')[0];
            $neighborhood_full_name = $node->text();
            $neighborhood_short_name = explode(' neighborhood ', $neighborhood_full_name)[0];
            if(!Neighborhood::where('name', '=', $neighborhood_short_name)->first()) return;
            print_f($neighborhood_short_name);
            $neighborhood = Goutte::request('GET', $neighborhood_link);
            sleep(4);
            $all_zipcodes = $neighborhood->filter('.content-item a');
            $zipcodes = '';
            $all_zipcodes->each(function ($nodes) use(&$zipcodes) {
                $zipcodes .= $nodes->html() . ',';
            });
            Neighborhood::create([
                'name' => $neighborhood_short_name,
                'zipcode' => $zipcodes
            ]);
        });
        echo "<h1>Complete</h1>";
    }

    /*
     * This function scrapes (https://www.thechicagoneighborhoods.com/neighborhoods/) for stock pics of Chicago neighborhoods
     */
    public function getNeighborhoodPictures()
    {
        $crawler = Goutte::request('GET', 'https://www.thechicagoneighborhoods.com/neighborhoods/?offset=1538859723700');
        $crawler->filter('.BlogList-item-title')->each(function ($node) { // Targets
            $hood_name = str_replace(' ', '', $node->text()) . '.jpg';
            if(file_exists(public_path('images/hoods/'.$hood_name))) return;
            $hood_link = $node->extract('href')[0];
            $hood_page = Goutte::request('GET', $hood_link);
            sleep(2);
            $hood_page->filter('noscript > img')->each(function ($node) use($hood_name) {
                $image_info = $node->extract(['src', 'alt'])[0];
                $image_url = $image_info[0];
                $image_file_name = $image_info[1] == '' ? $hood_name : $image_info[1];
                $filename = basename($image_file_name);
                Image::make($image_url)->save(public_path('images/hoods/' . $filename));
                echo "complete<br>";
            });
        });
    }

    public function imgToHood()
    {
        $images = scandir(public_path('images/hoods/'));
        $neighborhoods = Neighborhood::all();
        foreach($neighborhoods as $hood)
        {
            $hood_name = str_replace(' ', '', $hood->name) . '.jpg';
            if(in_array($hood_name, $images))
            {
                $h = Neighborhood::find($hood->id);
                $h->img_url = '/images/hoods/' . $hood_name;
                $h->save();
            }
        }
        echo "complete";
    }

    public function getChicagoParks()
    {
        $crawler = Goutte::request('GET', 'https://www.chicagoparkdistrict.com/parks-facilities?title=&field_location_type_target_id%5B0%5D=381');
        $crawler->filter('.thumbnail-object-title a')->each(function ($node) {
            $park_link = $node->extract('href')[0];
            $park_page = Goutte::request('GET', $park_link);
            $address = $this->getAddress($this->getElement($park_page, '.address'));
            $supervisor = $this->getSupervisor($this->getElement($park_page, '.location-header-label-supervisor'));
            $phone_number = $this->getPhoneNumber($this->getElement($park_page, '.phone'));
            echo $phone_number;exit;
        });
    }

    private function getElement($park_page, $filter)
    {
        $element = null;
        $park_page->filter($filter)->each(function ($node) use (&$element) {
           $element = $node;
        });
        return $element;
    }

    private function getAddress($element)
    {
        $exploded_address = explode('  ', trim($element->text()));
        $address = array_shift($exploded_address) .' '. array_pop($exploded_address);
        return $address;
    }

    private function getSupervisor($element)
    {
        return $element->siblings()->text();
    }

    private function getPhoneNumber($element)
    {
        $number = preg_replace("/[\r\n]+/", "\n", $element->text());
        $number = preg_replace("/\s+/", ' ', $number);
        $phone_number = explode('Main ', $number)[1];
        return $phone_number;
    }

}
