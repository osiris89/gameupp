<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Http\Controllers\GameController;
use App\Mail\Testing;


class HomeController extends Controller
{
    protected $gameController;
    public function __construct(GameController $gameController)
    {
        $this->gameController = $gameController;
        $this->middleware('auth');
    }

    public function sendMail()
    {
        Mail::to('robledoosiris@gmail.com')->send(new Testing());  
    }

    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todaysGames = $this->gameController->sortGames()['todaysGames'];
        $upcomingGames = $this->gameController->sortGames()['upcomingGames'];
        $hoods = $this->gameController->getNeighborhoods();
        return view('main.display_games', compact('hoods','todaysGames', 'upcomingGames'));
    }

}
