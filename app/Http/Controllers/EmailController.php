<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Testing;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function testEmail()
    {
        Mail::to('robledoosiris@gmail.com')->send(new Testing());
    }
}
