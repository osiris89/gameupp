<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['profile_picture', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
