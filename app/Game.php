<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = ['user_id', 'field_id', 'sport', 'players', 'number_of_players', 'votes', 'play_time', 'play_date'];
    
    public function field()
    {
        return $this->hasOne('App\Location', 'id', 'field_id');
    }
}
