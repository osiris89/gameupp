<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function games()
    {
        return $this->hasMany('App\Game');
    }

    public function neighborhood()
    {
        return $this->hasOne('App\Neighborhood', 'name', 'neighborhood');
    }
}
