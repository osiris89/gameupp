<!doctype html>
<html lang="en">
<head>
    @include('includes.head')
</head>
@include('includes.header')

<div class="container" id="app">
    <div class="card">
        <div class="card-header">
            <h5>Welcome to this Vue chatroom</h5>
            <span class="badge">@{{ usersInRoom.length }}</span>Currently in this room
        </div>
        <div class="card-body" style="padding: 0;">
            <div  style="width: 100%">

                <chat-log :messages="messages"></chat-log>
                <chat-composer current-user="{{ Auth::user()->name }}"  v-on:messagesent="addMessage"></chat-composer>

            </div>
        </div>
    </div>
</div>

@include('includes.footer')