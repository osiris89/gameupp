<!doctype html>
<html lang="en">
<head>
@include('includes.head')
    <style>
        body{
            background: #0B1544;
            margin: 0;
        }
        .top-nav{
            text-align: center;
            box-shadow: 1px 2px 10px #0F0E0F;
        }
        .nav-title{
            color: #E7E4EF;
            text-decoration: none;
            font-weight: 800;
            font-size: 25px;
        }
        .games-container{
            margin-top: 25px;
        }
        .row{
            margin: 0;
        }
        .game{
            margin: 1px;
            border: solid 1px white;
            border-radius: 5px;
            {{--background-image: url({{asset('images/hoods/LoganSquare.jpg')}});--}}
            min-height: 125px;
        }
        .game img {
            border-radius: 5px;
        }
        .count{
            position: absolute;
            top: -15px;
            left: 20px;
            color: white;
        }
        .thumbnail{
            background-color: #081033;
            padding: 0;
            border: none;
        }
        .user-image{
            border-radius: 50%;
            height: 90px;
            width: 90px;
        }
        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
        .player-images{
            margin-bottom: 20px;
        }
        .btn-vote{
            float: right;
        }
        .down_vote{
            margin-right: 5px;
        }
        .btn_directions{
            margin-top: 5px;
            text-align: right;
        }
        .descriptions{
            border: none;
            margin-bottom: 7px;
            margin-top: 7px;
        }
        .tab-pane{
            min-height: 200px;
        }
        .descriptions a{
            color: #E7E4EF;
        }
        .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
            border-radius: 10px;
        }
        .info-pane{
            position: relative;
        }
        .info-pane>h6,p{
            color: white;
        }
        .player-name {
            text-align: center;
            font-size: 10px;
        }
    </style>
</head>
<body>

@include('includes.header')
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                {{--@php print_f($game->field); @endphp--}}
                <div class="caption">
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs descriptions" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Map</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Rules</a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Parking</a></li>
                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Pictures</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <img class="img-responsive" src="https://maps.googleapis.com/maps/api/staticmap?center={{$game->field->lat}},{{$game->field->lng}}&zoom=17&size=5000x500&maptype=roadmap&markers=color:red%7C{{$game->field->lat}},{{$game->field->lng}}&key=AIzaSyCDa6QBm2_sAunMD27rznV8y59LDSF1OUc" alt="">
                                <div class="btn_directions">
                                    <a href="https://www.google.com/maps/search/?api=1&query={{$game->field->lat}},{{$game->field->lng}}" class="btn btn-success" role="button" target="_blank">Get Directions</a>
                                </div>
                                <div class="info-pane">
                                    <h6>{{$game->field->name}}</h6>
                                    <p>{{$game->field->address}}</p>
                                    <p>{{$game->field->city .", " . $game->field->zipcode}}</p>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <p>Donec et est ac nisi vehicula aliquet ut non nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent ultrices urna quis justo scelerisque, vitae auctor urna ultricies. Maecenas suscipit libero in feugiat mollis. Donec lobortis tristique tortor ac malesuada. In nec tellus id massa suscipit lobortis. Donec nec purus et ante euismod ullamcorper. Morbi in finibus ligula. Ut dignissim nisl diam, ac vulputate sem vestibulum sed.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="messages">
                                <p>Free street parking on Wrightwood Ave.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="settings">...</div>
                        </div>
                    </div>

                    <hr>
                    <h4>Players Going</h4>
                    <div class="row player-images">
                        @foreach($players as $player)
                            <div class="col-xs-4">
                                <img class="img-responsive user-image center" src="@if(!empty($player->profile['profile_picture'])){{asset('user_images/'.$player->profile['profile_picture'])}}@else{{asset('user_images/standard_image.png')}}@endif" title="{{$player->name}}">
                                <p class="player-name">{{$player->name}}</p>
                            </div>
                        @endforeach
                    </div>
                    <div class="row game-buttons">
                        <div class="col-xs-6"></div>
                        <div class="col-xs-6">
                            <button  @if ($voted) disabled @endif  class="btn up_vote btn-vote" data-user-id="{{Auth::user()->id}}" data-game-id="{{$game->id}}" data-vote-true="true">Vote In</button>
                            <button  @if ($voted) disabled @endif class="btn down_vote btn-vote" data-user-id="{{Auth::user()->id}}" data-game-id="{{$game->id}}" data-vote-false="false">Vote Out</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@include('includes.footer')

<script>
    $('#myTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    function downloadUrl(url, callback) {
        let request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                request.onreadystatechange = doNothing;
                callback(request, request.status);
            }
        };

        request.open('GET', url, true);
        request.send(null);
    }

    function doNothing() {
    }

    let up_vote_buttons = document.querySelectorAll('.up_vote');
    console.log(up_vote_buttons);
    up_vote_buttons.forEach(function(elem){
        elem.addEventListener('click', vote);
    });

    let down_vote_buttons = document.querySelectorAll('.down_vote');
    down_vote_buttons.forEach(function(elem){
        elem.addEventListener('click', vote);
    });

    function vote()
    {
        let vote_type = this.getAttribute('data-vote-true') == 'true' ? 'up_vote' : 'down_vote';
        console.log(vote_type);
        console.log(document.querySelector('.down_vote'));

        if(vote_type == 'up_vote')
        {
            document.querySelector('.down_vote').removeAttribute('disabled');
            document.querySelector('.up_vote').setAttribute('disabled', '');
        }
        else
        {
            document.querySelector('.up_vote').removeAttribute('disabled');
            document.querySelector('.down_vote').setAttribute('disabled', '');
        }
        let game_id = this.getAttribute('data-game-id');
        let user_id = this.getAttribute('data-user-id');
        let obj = {
            'user_id': user_id,
            'game_id': game_id,
            'vote_type': vote_type
        };
        // console.log(obj);return false;
        // updateVotes(game_id, vote_type);
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                let json_data = JSON.parse([ this.responseText ]);
                let votes = json_data.votes;

            }
        };
        xhttp.open("POST", "/updateGame", true);
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.setRequestHeader('X-CSRF-TOKEN', "{{ csrf_token() }}");
        xhttp.send(JSON.stringify(obj));
    }

    function updateVotes(game_id, vote_type)
    {
        let votes = parseInt(document.getElementById(game_id+'-votes').innerHTML);
        document.getElementById(game_id+'-votes').innerHTML =  vote_type == 'up_vote' ? ++votes : --votes;
    }

</script>
</body>
</html>