<!doctype html>
<html lang="en">
<head>
    @include('includes.head')
    <title>List Games</title>
    <style>
        body{
            background: #0B1544;
            margin: 0;
        }
        .top-nav{
            text-align: center;
            box-shadow: 1px 2px 10px #0F0E0F;
        }
        .nav-title{
            color: #E7E4EF;
            text-decoration: none;
            font-weight: 800;
            font-size: 25px;
        }
        .games-container{
            margin-top: 25px;
        }
        .row{
            margin: 0;
        }
        .game{
            margin: 1px;
            border: solid 1px white;
            border-radius: 5px;
            {{--background-image: url({{asset('images/hoods/LoganSquare.jpg')}});--}}
            min-height: 125px;
        }
        .game img {
            border-radius: 5px;
        }
        .count{
            position: absolute;
            top: -15px;
            left: 20px;
            color: white;
        }
        .thumbnail{
            padding: 0;
            border: none;
        }
    </style>
</head>
<body>
    {{--<nav class="top-nav">--}}
        {{--<a href="/" class="nav-title">{{$hood['name']}}</a>--}}
    {{--</nav>--}}
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="{{asset($hood['img_url'])}}" alt="...">
                <div class="caption">
                    <img class="img-responsive" src="https://maps.googleapis.com/maps/api/staticmap?center={{$hood['name']}},Chicago,IL&zoom=14&size=600x300&maptype=roadmap
&markers=color:blue%7Clabel:S%7C{{$hood['field'][0]['lat']}},{{$hood['field'][0]['lng']}}&key=AIzaSyCDa6QBm2_sAunMD27rznV8y59LDSF1OUc" alt="">
                    <h3>Thumbnail label</h3>
                    <p>...</p>
                    <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
