<!doctype html>
<html lang="en">
<head>
    @include('includes.head')
    <title>New Design</title>
    <style>
        body{
            background: #0B1544;
            margin: 0;
        }
        .top-nav{
            text-align: center;
            box-shadow: 1px 2px 10px #0F0E0F;
        }
        .nav-title{
            color: #E7E4EF;
            text-decoration: none;
            font-weight: 800;
            font-size: 25px;
        }
        .games-container{
            margin-top: 25px;
        }
        .row{
            margin: 0;
        }
        .game{
            margin: 1px;
            border: solid 1px white;
            border-radius: 5px;
            {{--background-image: url({{asset('images/hoods/LoganSquare.jpg')}});--}}
            min-height: 125px;
        }
        .game img {
            border-radius: 5px;
        }
        .count{
            position: absolute;
            top: -15px;
            left: 20px;
            color: white;
        }

    </style>
</head>
<body>
    <!------Top Nav------->
    <nav class="top-nav">
        <a href="/" class="nav-title">Today's Games</a>
    </nav>
    <!-----Game Cards Container------->
    <div class="games-container">
        <div class="row">
            @foreach($hoods as $hood)
            <div class="col-xs-6">
                <div class="game">
                    <a href="/hood/{{$hood['id']}}"><img src="{{asset($hood['url'])}}" alt="" class="img-responsive"/></a>
                    <h3 class="count">{{$hood['count']}}</h3>
                </div>
            </div>
            @endforeach
        </div>
    </div>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>