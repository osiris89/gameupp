<!doctype html>
<html lang="en">
<head>
    @include('includes.head')
    <link rel="stylesheet" href="{{asset('css/user_page.css?v=1.004')}}">
    <style>
        div {
            background-color: initial;
        }

        #create_games_form {
            width: 90%;
            margin-top: 20px;
            margin-bottom: 90px;
            margin-left: 15px;
            color: yellow;
        }

        #map {
            display: block;
            height: 100%;
        }

        #map-div
        {
            height: 1px;
            width: 100%;
        }
    </style>
    <title>Create a game!</title>
</head>

@include('includes.header')
<h3>Create a game!</h3>
<div id="map-div">
    <div id="map"></div>
</div>


<form method="POST" class="form-horizontal"  id="create_games_form" action="/create_game">
    {{csrf_field()}}
    <div class="form-group">
        <label for="sport" class="col-sm-2 control-label">Select sport:</label>
        <div class="col-sm-10">
            <select class="form-control" id="select_sport" name="sport" required>
                <option disabled><---Favorite Sports---></option>
                @foreach($sports as $sport)
                    <option>{{$sport}}</option>
                @endforeach
                <option disabled><---Other Sports---></option>
                @foreach($other_sports as $other_sport)
                    <option>{{$other_sport}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-5 col-xs-8">
                <label for="field_name" class="col-sm-2 control-label">Select a location:</label>
                <select class="form-control" name="field_id" id="field_selections" required>
                   
                @foreach($locations as $location)
                    <option value="{{$location->id}}"> {{$location->name}} </option>
                @endforeach
                    <option id='new_field' value="new" selected> New </option>
                </select>
            </div>
            <div class="col-sm-5 col-xs-2">
                <a href="#" class="btn btn-primary" style="font-size: 20px; position: relative; top: 20px;" onclick="mapToggleScreen()">
                    <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="type" class="col-sm-2 control-label">Location Type</label>
        <div class="col-sm-10">
            <select class="form-control" name="type" required>
                <option value="park"> Park </option>
                <option value="facility"> Facility </option>
                <option value="business"> Business </option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="participants" class="col-sm-2 control-label">Select # of participants:</label>
        <div class="col-sm-10">
            <select class="form-control" name="number_of_players" required>
                <option value="0"> Unlimited </option>
                @for($i=1; $i<=50; $i++)
                    <option value="{{$i}}"> {{$i}} </option>
                @endfor
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="date" class="col-sm-2 control-label">Select a date:</label>
        <div class="col-sm-10">
            <input type="date" class="form-control" name="play_date">
        </div>
    </div>

    <div class="form-group">
        <label for="time" class="col-sm-2 control-label">Select a time:</label>
        <div class="col-sm-10">
            <input type="time" class="form-control" name="play_time">
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Create Game!</button>
</form>


<script>
    let map;
    let marker;
    let infowindow;
    let messagewindow;
    let latlng;
    let address, city, state, district, zipcode, lat, lng;

    function initMap() {
        let chicago = {lat: 41.9, lng: -87.706009};
        let geocoder = new google.maps.Geocoder;

        map = new google.maps.Map(document.getElementById('map'), {
            center: chicago,
            zoom: 11,
            mapTypeControl: false
        });

        infowindow = new google.maps.InfoWindow();

        messagewindow = new google.maps.InfoWindow({
            content: "Location Created!"
        });

        // Populates the map with existing fields.
        downloadUrl('/api/fields', function (data) {
            let fields = JSON.parse(data);
            for (let x in fields)
            {
                (function () {
                    let field = fields[x];
                    let id = field.id;
                    let name = field.name;
                    let address = field.address;
                    let type = field.type;
                    let point = new google.maps.LatLng(
                        parseFloat(field.lat),
                        parseFloat(field.lng));

                    let game_info = "<table>" +
                        "<tr><td>Location:</td><td>" + name + "</td></tr>" +
                        "<tr><td>Address:</td><td>" + address + "</td> </tr>" +
                        "<tr><td><button class='btn btn-primary' id="+id+" onclick='selectField("+id+")'>Select This Location</button></td></tr>" +
                        // "<tr><td>Area:</td><td>" + neigh + "</td> </tr>" +
                        "</table>";

                    let icon =  {};
                    let marker = new google.maps.Marker({
                        map: map,
                        position: point,
                        label: icon.label
                    });
                    marker.addListener('click', function () {
                        infowindow.setContent(game_info);
                        infowindow.open(map, marker);
                    });
                })();
            }
        });


        /*
         * This function, for the most part, provides the data of the location where a user clicks on the map. It also sets a basic info window for the location.
         */
        google.maps.event.addListener(map, 'click', function (event) {
            lat = parseFloat(event.latLng.lat().toFixed(6));
            lng = parseFloat(event.latLng.lng().toFixed(6));
            latlng = {lat: lat, lng: lng };

            geocoder.geocode({'location': latlng}, function(results, status) {
                let location = results[0].address_components;
                location.coordinates = latlng;
                address = location[0].short_name + " " + location[1].short_name;
                city = location[3].short_name;
                state = location[5].short_name;
                district = location[2].short_name;
                zipcode = location[7].short_name;
                let form = "<div id=\"form\">\n" +
                                "<table>\n" +
                                    "<tr><td>Name:</td> <td><input type='text' id='fieldName' name=\"fieldName\"/> </td> </tr>\n" +
                                    "<tr><td>Address:</td> <td><input type='text' id='address' name=\"address\" value='"+address+"'/> </td> </tr>\n" +
                                    "<tr><td>Zipcode:</td> <td><input type='text' id='zipcode' name=\"zipcode\" value='"+zipcode+"'/></td> </tr>\n" +
                                    "<tr><td>Surface Type:</td> <td><select id='surface_type' name=\"surface\" onchange=\"newSurface()\"> " +
                                    "<option value=\"Turf\">Turf</option> " + 
                                    "<option value=\"Grass\">Grass</option> " +
                                    "<option value=\"Cement\">Cement</option> " +
                                    "<option value=\"Hardground\">Indoor (Hardground)</option> " +
                                    "<option value=\"Hardwood\">Indoor (Hardwood)</option> " +
                                    "<option value=\"ITurf\">Indoor (Turf)</option> " +
                                    "</td> </tr>\n" +
                                    "<tr><td>Area:</td> <td><input type='hidden' id='area' name='area' value='" + district + "'/>" + district + "</td> </tr>\n" +
                                    "<tr><td><a class='btn btn-primary' onclick='selectField(0)'>Select This Location</a></td></tr>\n" +
                                "</table>\n" +
                           "</div>";
                infowindow.setContent(form);
                infowindow.open(map, marker);

                if (status !== 'OK'){ window.alert('Geocoder failed due to: ' + status); }
                
                /*
                * CREATE OR UPDATE DATA INPUTS
                */
                if(document.getElementById('data_container') === null)
                {
                    createNewLocationDataInputs(location);
                }
                else
                {
                    updateNewLocationDataInputs(location);
                }
            });

            if (marker) {
                //if marker already was created change position
                marker.setPosition(event.latLng);
            } else {
                //create a marker
                marker = new google.maps.Marker({
                    position: event.latLng,
                    map: map,
                    // draggable: true
                });
            }

        });
    }

    function saveData() {
        let name = escape(document.getElementById('name').value);
        let address = escape(document.getElementById('address').value);
        let zipcode = document.getElementById('zipcode').value;
        let area = document.getElementById('area').value;
        let latlng = marker.getPosition();
        let url = '/api/new_field?name=' + name + '&address=' + address + '&area=' + area +
            '&zipcode=' + zipcode + '&lat=' + parseFloat(latlng.lat()).toFixed(6) + '&lng=' + parseFloat(latlng.lng()).toFixed(6);

        downloadUrl(url, function (data, responseCode) {
            if (responseCode == 200 ) {
                infowindow.close();
                messagewindow.open(map, marker);
                location.reload();
            }
        });
    }

    function downloadUrl(url, callback) {
        let request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                request.onreadystatechange = doNothing;
                callback(request.responseText, request.status);
            }
        };

        request.open('GET', url, true);
        request.send(null);
    }

    function doNothing() {
    }

    /*
        Kill me..........
    */
    function selectField(field_id, location)
    {
        let field_selections = document.querySelector('#field_selections').children;
        let len = field_selections.length;
        if(field_id == 0)
        {
            let field_name = document.querySelector('#fieldName').value;
            field_selections[len-1].setAttribute('selected', 'selected');
            field_selections[len-1].innerHTML = "New (" + field_name + ")";
            field_selections[len-1].setAttribute('value', field_name);
        }
        else
        {
            for(let i=0; i<len; i++)
            {
                if(field_id == field_selections[i].value)
                {
                    field_selections[i].setAttribute('selected', 'selected');
                }

                else
                {
                    field_selections[i].removeAttribute('selected');
                }
            }
        }
        mapToggleScreen()
    }

    function mapToggleScreen()
    {
        let buttons = document.querySelectorAll('button');
        let len = buttons.length;
        for(let i = 0; i<len; i++)
        {
            if(buttons[i].getAttribute('title') == 'Toggle fullscreen view')
            {
                buttons[i].click();
            }
        }
    }

    function createCoordinateInputs(coordinates, data_container)
    {
        let lat = coordinates.lat;
        let lng = coordinates.lng;
        let lat_input = document.createElement("input");
        lat_input.setAttribute('id', 'latt');
        lat_input.setAttribute('type', 'hidden');
        lat_input.setAttribute('name', 'lat');
        lat_input.setAttribute('value', lat);
        let lng_input = document.createElement("input");
        lng_input.setAttribute('id', 'lngg');
        lng_input.setAttribute('type', 'hidden');
        lng_input.setAttribute('name', 'lng');
        lng_input.setAttribute('value', lng);
        data_container.appendChild(lat_input);
        data_container.appendChild(lng_input);
    }

   /*
    * We create new hidden inputs and add them to the 'create a game' form. The hidden inputs contain the values
    * of the 'new' field selected from the google map. These values are then passed with the form to create the new
    * field in the database.
    *
    * var location (array) Array of address information pertaining to the location selected by the user in the map.   
    */
    function createNewLocationDataInputs(location)
    {
        let new_element, data;
        let coordinates = location.coordinates;
        delete location.coordinates; // We delete the coordinates from the object. Otherwise the iteration below will break. DEFINITELY NEEDS REFACTORING!!
        let game_form = document.getElementById('create_games_form');
        let len = location.length;
        let data_container = document.createElement("div");
        data_container.setAttribute('id', 'data_container'); // We create a container for all of the hidden inputs. This allows us to conveniently target them.
        createCoordinateInputs(coordinates, data_container);
        game_form.appendChild(data_container);
        for(i = 0; i < len; i++)                             // Begins creating the hidden inputs and adding them to the data_container 
        {
            data = location[i];
            data_label = changeLabelName(location[i].types[0]);
            new_element = document.createElement("input");
            new_element.setAttribute('id', data_label);
            new_element.setAttribute('type', 'hidden');
            new_element.setAttribute('name', 'location['+data_label+']');
            new_element.setAttribute('value', data.short_name);
            data_container.appendChild(new_element);
        }
        let surface_type = document.getElementById('surface_type').value;
        let surface = document.createElement("input");
        surface.setAttribute('type', 'hidden');
        surface.setAttribute('name', 'surface');
        surface.setAttribute('id', 'surface_type_final');
        surface.setAttribute('value', surface_type);
        data_container.appendChild(surface);
    }

   /*
    * We use this function to update the hidden input values created by createNewLocationDataInput().
    * Otherwise the hidden input values would be stuck with the first custom location a user selects on the map.
    * This allows the user to click around the map multiple times and the values will be updated accordingly.
    * 
    * var location (array) Array of address information pertaining to the location selected by the user in the map.  
    */
    function updateNewLocationDataInputs(location)
    {
        let coordinates = location.coordinates;
        delete location.coordinates;
        let data_container = document.getElementById('data_container');
        let len = data_container.childNodes.length;
        for(i = 0; i < 8; i++)
        {
            data = location[i];
            data_label = changeLabelName(location[i].types[0]);
            data_input = document.getElementById(data_label);
            data_input.setAttribute('value', data.short_name);
        }
        let lat = document.getElementById('latt');
        lat.setAttribute('value', coordinates.lat);
        let lng = document.getElementById('lngg');
        lng.setAttribute('value', coordinates.lng);
       
    }

    // Google labels the following values as such. This function simply changes them to be more readable.
    function changeLabelName(label)
    {
        switch(label) {
            case 'administrative_area_level_1':
                return 'state';
            case 'administrative_area_level_2':
                return 'county';
            case 'locality':
                return 'city';
            case 'route':
                return 'street_name'
            default:
                return label;
        }
    }

    function newSurface()
    {
        let surface = document.getElementById('surface_type').value;
        let data_container = document.getElementById('data_container');
        let surface_type_final = document.getElementById('surface_type_final');
        surface_type_final.setAttribute('value', surface);
    }

</script>


<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDa6QBm2_sAunMD27rznV8y59LDSF1OUc&callback=initMap">
</script>

@include('includes.footer')