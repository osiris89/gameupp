@extends('layouts.app')
@section('user_profile')
<div class="all">
    <!----------------------Main Section---------------------->
    <div class="container-fluid">
        <div class="top-section">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 user_info">
                    <div>

    <img class="img-responsive" src="@if(!empty($profile_picture)){{asset('user_images/'.$profile_picture)}}@else{{asset('user_images/standard_image.png')}}@endif" alt=""
         width="100%">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 no-padding">
                    <div class="row no-padding">
                        <div class="col-md-6 col-sm-6 col-xs-6 no-padding full divider-inside-top">
                            <table class="table user_info_table borderless">
                                <thead>
                                <tr>
                                    <th>
                                        Name: {{$user_name}}
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Age:
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Location: {{$user_info->zipcode}}
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Sports: {{$user_info->sports}}
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Squad:
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="row no-padding">
                        <div class="col-md-6 col-sm-6 col-xs-6 no-padding full divider-inside-top">
                            <div class="about_me">
                                <p><strong>About Me:</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <!-----------------Middle Section------------------------->
    <div class="middle_section">
        <div class="container mid-section">
            <div class="middle-game-buttons">
                <a href="/create_game"><h3>CREATE GAME</h3></a>
            </div>
            <div class="middle-game-buttons">
                <a href=""><h3>MY GAMES</h3></a>
            </div>
            <div class="middle-game-buttons">
                <a href=""><h3>MY TEAM</h3></a>
            </div>
        </div>
    </div>

    <!----------------Photo Section---------------------------->
    <br><br>
    <div class="photo-gallery" style="background: transparent;">
        <div class="btn-group" role="group">
            <form action="{{action('PostsController@photoUpload')}}" method="POST" enctype="multipart/form-data" id="form">
                <label id="file_label" for="photo-upload" style="background-color: rgb(44,42,49)" class="btn btn-default btn-lg" onclick="openNav()" >
                <span aria-hidden="true">Upload Picture</span>
                </label>
                <input id="photo-upload" name="image-upload" type="file" style="display: none;"/>

                <div id="myNav" class="overlay" style="z-index: 33;">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <div class="overlay-content">
                        <a href="#">Image Preview</a>
                        <img class="img-responsive" id="blah" src="#" alt="your image" style="max-height: 500px; margin: 0 auto;" />
                        <input type="checkbox" name="profile_picture"> <span>Make Default Picture?</span><br>
                        <button type="submit">Upload</button>
                    </div>
                </div>
                {{ csrf_field() }}
            </form>
        </div>
        <h3>Photo Gallery</h3>
        <div class="row" style="background-color: transparent;">
        @foreach($photos as $photo)
            <div class="column" style="background-color: transparent;">
                <img src="{{asset('user_images/'.$photo->file_path)}}" onclick="openModal();currentSlide({{$loop->iteration}})" class="img-responsive hover-shadow">
            </div>
        @endforeach
        </div>

        <div id="myModal" class="modal">
            <span class="close cursor" onclick="closeModal()">&times;</span>
            <a id="delete-photo" href="#" class="btn btn-danger">Delete Photo</a>
            <div class="modal-content">
            @foreach($photos as $photo)
                <div class="mySlides" id="user-image-{{$photo->id}}">
                    <div class="numbertext">{{$loop->index + 1}} / {{count($photos)}}</div>
                    <img  src="{{asset('user_images/'.$photo->file_path)}}" style="width:100%">
                </div>
            @endforeach

                <!-- Next/previous controls -->
                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>

                <!-- Caption text -->
                <div class="caption-container">
                    <p id="caption"></p>
                </div>
            @foreach($photos as $photo)
                <!-- Thumbnail image controls -->
                <div class="column">
                    <img class="img-responsive demo" src="{{asset('user_images/'.$photo->file_path)}}" onclick="currentSlide({{$loop->iteration}})" alt="Nature">
                </div>
            @endforeach
            </div>
        </div>
    </div>


    <!-------------Bottom Navigation Bar---------------------->
    @include('includes.footer') 
</div>
<script>
    // Open the Modal
    function openModal() {
        document.getElementById('myModal').style.display = "block";
    }

    // Close the Modal
    function closeModal() {
        document.getElementById('myModal').style.display = "none";
    }

    var slideIndex = 1;
    showSlides(slideIndex);

    // Next/previous controls
    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    // Thumbnail image controls
    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        captionText.innerHTML = dots[slideIndex-1].alt;
    }
</script>

@endsection


