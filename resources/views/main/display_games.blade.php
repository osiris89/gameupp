@extends('layouts.app')
@section('games')
<style>
    .game-cards{

    }
    .game-card{
        background-color: #081033;
        /*border: 1px solid white;*/
        border-radius: 5px;
        height: 150px;
        width: 100%;
        margin-top: 15px;
        /*box-shadow: 1px 2px 10px darkgrey;*/
    }
    .game-icon{
        /*border: solid yellow 1px;*/
        height: 100%;
    }
    .game-info-container{
        height: 100%;
    }
    .game-info-row{
        position: relative;
        /*border: lightblue solid 1px;*/
        height: 50%;
    }
    .game-info{
        position: relative;
        top: 25px;
    }
    .game-info > p{
        color: white;
        font-weight: bold;
        font-size: 16px;
    }
    .game-voting{
        position: absolute;
        bottom: 10px;
        left: 0;
    }
    .btn-vote{
        width: 75px;
        border-radius: 25px;
    }
    .main-content{
        margin-left: 10px;
        margin-right: 10px;
    }
    .more-info{
        position: absolute;
        bottom: 7px;
        right: 15px;
    }
    .glyphicon{
        top: 0;
        font-size: 22px;
    }
    .sports-icon{
        position: relative;
        top: 40px;
        height: 50%;
        margin-left: auto;
        margin-right: auto;
    }
    .well {
        background-color: #081033;
        border: none;
        box-shadow: inset 0 5px 5px rgba(0,0,0,.4);
    }
    .btn-vote{
        font-weight: bolder;
    }
    .up_vote{
        background-color: #1F3CBF;
        border: none;
        color: #E7E4EF;
    }
    .down_vote{
        background-color: #0A1440;
        border: none;
        color: #E7E4EF;
    }
    .view-game{
        float: right;
        background-color: #0B1544;
        color: #E7E4EF;
        border-radius: 20px;
        position: relative;
        top: 10px;
        left: 10px;
        border: solid 1px #E7E4EF;
    }
    .top-nav {
        height: 75px;
    }
    .nav>li>a {
        top: 15px;
        font-size: 15px;
        border: none;
    }
    .nav-tabs {
        height: 100%;
        border: none;
    }
    .nav-tabs>li {
        width: 50%;
        height: 100%;
    }
    .nav>li>a:focus, .nav>li>a:hover {
        background-color: inherit;
        color: #E7E4EF;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
        color: #E7E4EF;
        background-color: #081033;
        border: none;
    }
    li.active {
        background-color: #081033;
    }

    #top-nav-container {
        position: sticky;
        top: 350px;
        z-index: 2;
    }

    .descriptions {
        background-color: #0B1544;
    }
</style>
<!----------- Google Map ------------->
<div id='map'></div>
<!----------- Today's Games Header ----------->
<div id="top-nav-container">
    <nav class="top-nav">
        <ul class="nav nav-tabs descriptions" role="tablist">
            <li role="presentation" class="active">
                <a href="#todaysGames" aria-controls="todaysGames" role="tab" data-toggle="tab">Today's Games</a>
            </li>
            <li role="presentation">
                <a href="#upcomingGames" aria-controls="upcomingGames" role="tab" data-toggle="tab">Upcoming Games</a>
            </li>
        </ul>
    </nav>
</div>

<!---------- Today's Games --------------------->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="todaysGames">
        <div class="btn_directions">
            <div class="main-content">
                <div class="game-cards">
                    @if (count($todaysGames) <= 0)
                        <h2>No games today yet!</h2>
                    @endif
                    @foreach($todaysGames as $game) <!----Data from HomeController->index()---->
                    <div class="game-card">
                        <div class="col-xs-4 game-icon">
                            <img src="{{asset("images/sports_icons/$game->sport.png")}}" alt="" class="img-responsive sports-icon">
                        </div>
                        <div class="col-xs-8 game-info-container">
                            <div class="row game-info-row">
                                <div class="col-xs-*">
                                    <div class="game-info">
                                        <p>{{$game->sport}}</p>
                                        <p>{{$game->play_time}}</p>
                                        <p>{{$game->field->neighborhood_name}}</p>
                                        <p>Current votes: {{$game->votes}}@if($game->number_of_players != 0)/{{$game->number_of_players}} @endif</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row game-info-row">
                                <div class="col-xs-*">
                                    <div class="more-info">
                                        <a data-toggle="collapse" href="#collapse{{$game->id}}">
                                            <i class="fas fa-chevron-circle-down"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--------More Information-------->
                    <div class="collapse" id="collapse{{$game->id}}">
                        <div class="well">
                            <div class="row">
                                <div class="col-xs-4">
                                    <h4>Players Going</h4>
                                </div>
                                <div class="col-xs-8">
                                    @foreach($game['attending'] as $attendee)
                                        <h5>{{$attendee->name}}</h5>
                                    @endforeach
                                    <a class="btn view-game" href="/games/{{$game->id}}">View Game</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="upcomingGames">
        <div class="btn_directions">
            <div class="main-content">
                <div class="game-cards">
                @foreach($upcomingGames as $game) <!----Data from HomeController->index()---->
                    <div class="game-card">
                        <div class="col-xs-4 game-icon">
                            <img src="{{asset("images/sports_icons/$game->sport.png")}}" alt="" class="img-responsive sports-icon">
                        </div>
                        <div class="col-xs-8 game-info-container">
                            <div class="row game-info-row">
                                <div class="col-xs-*">
                                    <div class="game-info">
                                        <p>{{$game->sport}}</p>
                                        <p>{{$game->play_time}}</p>
                                        <p>{{$game->field->neighborhood_name}}</p>
                                        <p>Current votes: {{$game->votes}}@if($game->number_of_players != 0)/{{$game->number_of_players}} @endif</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row game-info-row">
                                <div class="col-xs-*">
                                    <div class="more-info">
                                        <a data-toggle="collapse" href="#collapse{{$game->id}}">
                                            <i class="fas fa-chevron-circle-down"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--------More Information-------->
                    <div class="collapse" id="collapse{{$game->id}}">
                        <div class="well">
                            <div class="row">
                                <div class="col-xs-4">
                                    <h4>Players Going</h4>
                                </div>
                                <div class="col-xs-8">
                                    @foreach($game['attending'] as $attendee)
                                        <h5>{{$attendee->name}}</h5>
                                    @endforeach
                                    <a class="btn view-game" href="/games/{{$game->id}}">View Game</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>


<!----------- Neighborhood Cards ----------->


<script>
    function initMap() {
        let map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(41.9, -87.706009),
            zoom: 11,
            mapTypeControl: false,
            zoomControl: false
        });
        let infoWindow = new google.maps.InfoWindow;
        let all_markers = [];
        let games = @php echo json_encode(array_merge($todaysGames, $upcomingGames)); @endphp ;
        for (let x in games){(function ()
        {
            let game = games[x];
            let field = games[x].field;
            let id = field.id;
            let name = field.name;
            let address = field.address;
            let type = field.type;
            let votes = game.votes;
            let neigh = field.neighborhood;
            let game_time = tConvert(game.play_time);
            let point = new google.maps.LatLng(
                parseFloat(field.lat),
                parseFloat(field.lng)
            );

            let sum_coordinates = field.lat + field.lng;
            if(all_markers.includes(sum_coordinates))
            {
                
            }
            else
            {
                all_markers.push(sum_coordinates);
                let game_info = "<table>" +
                                    "<tr><td>Location:</td><td>" + name + "</td></tr>" +
                                    "<tr><td>Address:</td><td>" + address + "</td> </tr>" +
                                    "<tr><td>Participants:</td><td>" + votes + "</td> </tr>" +
                                    "<tr><td>Area:</td><td>" + neigh + "</td> </tr>" +
                                    "<tr><td>Game Time:</td><td>" + game_time + "</td> </tr>" +
                                "</table>";

                

                let marker = new google.maps.Marker({
                    map: map,
                    position: point,
                    label: game.sport
                });
                marker.addListener('click', function () {
                    infoWindow.setContent(game_info);
                    infoWindow.open(map, marker);
                });
            }
        })();
        }
    }

    function tConvert (time) {
        // Check correct time format and split into components
        time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice (1);  // Remove full string match value
            time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join (''); // return adjusted time or original string
    }


    function downloadUrl(url, callback) {
        let request = window.ActiveXObject ?
                new ActiveXObject('Microsoft.XMLHTTP') :
                new XMLHttpRequest;

        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                request.onreadystatechange = doNothing;
                callback(request, request.status);
            }
        };

        request.open('GET', url, true);
        request.send(null);
    }

    function doNothing() {
    }

    let up_vote_buttons = document.querySelectorAll('.up_vote');
    up_vote_buttons.forEach(function(elem){
        elem.addEventListener('click', vote);
    });

    let down_vote_buttons = document.querySelectorAll('.down_vote');
    down_vote_buttons.forEach(function(elem){
        elem.addEventListener('click', vote);
    });

    function vote()
    {
        let vote_type = this.getAttribute('data-vote') == 'true' ? 'up_vote' : 'down_vote';
        console.log(vote_type);

        vote_type == 'up_vote' ? this.nextElementSibling.removeAttribute('disabled') : this.previousElementSibling.removeAttribute('disabled');
        this.setAttribute('disabled', '');
        let game_id = this.getAttribute('data-game-id');
        let user_id = this.getAttribute('data-user-id');
        let obj = {
            'user_id': user_id,
            'game_id': game_id,
            'vote_type': vote_type
        };
        // console.log(obj);return false;
        // updateVotes(game_id, vote_type);
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                let json_data = JSON.parse([ this.responseText ]);
                let votes = json_data.votes;
                
           }
        };
        xhttp.open("POST", "/updateGame", true);
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.setRequestHeader('X-CSRF-TOKEN', "{{ csrf_token() }}");
        xhttp.send(JSON.stringify(obj));
    }

    function updateVotes(game_id, vote_type)
    {
        let votes = parseInt(document.getElementById(game_id+'-votes').innerHTML);
        document.getElementById(game_id+'-votes').innerHTML =  vote_type == 'up_vote' ? ++votes : --votes;
    }


</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDa6QBm2_sAunMD27rznV8y59LDSF1OUc&callback=initMap">
</script>
@endsection

