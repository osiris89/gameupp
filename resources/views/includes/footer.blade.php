@if(Auth::check())
    <style>
        .button-name {
            position: relative;
            bottom: 10px;
            font-size: 10px;
        }
        .btn-group-justified {
            text-align: center;
        }
        .fas {
            font-size: 20px;
        }
    </style>
<div class="navs bottom-nav">
    <div class="divider-inside-top">
        <div class="btn-group btn-group-justified" role="group" aria-label="Left Align">
            <div class="btn-group" role="group">
                <a href={{url("/home")}}>
                    <button type="button" class="btn btn-default btn-lg">
                        <i class="fas fa-user-alt"></i>
                    </button>
                    <p class="button-name">Profile</p>
                </a>

            </div>
            <div class="btn-group" role="group">
                <a href={{url("/")}}>
                    <button type="button" class="btn btn-default btn-lg">
                        <i class="fas fa-search"></i>
                    </button>
                    <p class="button-name">Search</p>
                </a>

            </div>
            <div class="btn-group" role="group">
                <a href="/friends">
                    <button type="button" class="btn btn-default btn-lg">
                        <i class="fas fa-users"></i>
                    </button>
                    <p class="button-name">Friends</p>
                </a>
            </div>
            <div class="btn-group" role="group">
               <a href={{url("/messages")}}>
                    <button type="button" class="btn btn-default btn-lg">
                       <i class="fas fa-comment-dots"></i>
                    </button>
                    <p class="button-name">Messages</p>
               </a>
            </div>
        </div>
    </div>
</div>
@else  <div class="row" style="margin-bottom: 5px;">
    <div class="col-xs-12 col-sm-12">
        <a href={{url("/login")}}><button class="btn" style="width: 100%; background-color: yellow; color: black;">Log In</button></a>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <a href={{url("/register")}}><button class="btn" style="width: 100%; background-color: transparent; color: white; border: solid 1px white;">Sign Up</button></a>
    </div>
</div>
@endif

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/custom.js?v=1.01') }}"></script>
