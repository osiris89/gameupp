<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
<meta name="theme-color" content="#0B1544">
<link rel="manifest" href="{{asset('manifest.json')}}">

<!-- Styles -->
{{--<link rel='stylesheet' type='text/css' href="{{asset('css/start_screen.css')}}?v={{env('CSS_V')}}" >--}}
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link href="{{asset('css/app.css')}}" rel="stylesheet">
<link href="{{asset('css/custom.css?v=1.0.0.2')}}" rel="stylesheet">

