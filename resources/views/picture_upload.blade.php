<!doctype html>
<html lang="en">
<head>
    <title>Gameupp</title>
    @include('includes.head')
</head>
<body>
    @include('includes.header')
    <h4 style="text-align: center;">Image Preview</h4>

    <h5>Would you like to upload this picture to your gallery?</h5>

</body>
<script>
    document.getElementById('photo-upload').disabled = true;
</script>
</html>