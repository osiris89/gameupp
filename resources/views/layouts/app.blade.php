<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('includes.head')
    </head>
    <body>
    @include('includes.header')
        @yield('games')
        @yield('user_profile')
        @yield('choose_sport')
    @include('includes.footer')
    </body>
</html>
