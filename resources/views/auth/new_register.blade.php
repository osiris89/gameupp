<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <title>Sign Up</title>
    <style>
        html, body {
            height: 100%;
            background-color: black;
        }

        .carousel, .item, .active {
            height: 100%;
        }

        .carousel-inner {
            height: 100%;
        }

        .carousel-inner > .item > a > img, .carousel-inner > .item > img {
            height: 100%;
        }

        h1 {
            text-align: center;
            color: yellow;
        }

        .form-1 {
            height: 100%;
        }

        label {
            color: yellow;
        }
    </style>
</head>
<body>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{asset('js/custom.js')}}"></script>

<form action="{{ route('user_info_register') }}" method="POST" class="form-1">
    <!-- Wrapper for slides -->
    <h1>Lets create your account!</h1>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name" class="col-md-4 control-label">Name or Username</label>
        <div class="col-md-6">
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
            @if ($errors->has('name'))
                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
        <div class="col-md-6">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="col-md-4 control-label">Password</label>
        <div class="col-md-6">
            <input id="password" type="password" class="form-control" name="password" required>
            @if ($errors->has('password'))
                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
        <div class="col-md-6">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        </div>
    </div>
    {{csrf_field()}}
    <button type="submit">Submit</button>
</form>
</body>
</html>