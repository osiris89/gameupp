@if(Auth::user() )
    <script>window.location.href = "/home"</script>
@endif
<head>
    @include('includes.head')
    <style>
        @media only screen and (min-width: 768px) {
            #logo {
                width: 75%;
            }
            .img-container{
                text-align: -webkit-center;
            }
        }
        @media (min-width: 768px) {
            .container {
                width: 800px;
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="img-container">
            <img class="img-responsive" src="images/logo.gif" id="logo">
        </div>
        <form class="form-horizontal loggin" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div>
                    <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Username" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div>
                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div>
                    <button type="submit" class="btn btn-lg btn-block lss" id="login">
                        Log In
                    </button>
                    <div class="login_signup">
                        <a href="{{ route('register') }}"><button type="button" class="btn btn-secondary btn-lg btn-block lss" id='signup'>Sign Up</button></a>
                    </div>

                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
                </div>
            </div>
        </form>
    </div>
</body>
