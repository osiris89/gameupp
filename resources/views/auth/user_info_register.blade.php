
<!doctype html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalabel=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <title>Sign Up</title>
    <style>
        html, body {
            height: 100%;
            background-color: darkslategray;
            overflow: hidden;
            margin: 0px;
        }

        .back-container {
            height: 325px;
            background-color: yellow;
            transform: rotate(-23deg);
            margin-top: -47px;
            margin-left: -197px;
            width: 665px;
            position: static;
        }

        .carousel, .item, .active {
            height: 100%;
        }

        .carousel-inner {
            height: 100%;
        }

        .carousel-inner > .item > a > img, .carousel-inner > .item > img {
            height: 100%;
        }

        h1, h6 {
            text-align: center;
            color: black;
        }


        .container {
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        label {
            color: yellow;
        }

        .favorite_sports_label {
            width: 100%;
            height: 100px;
            background-color: darkslategray;
            text-align: center;
            box-shadow: 15px 4px 40px 0 rgba(0, 0, 0, 0.7), 40px 6px 20px 0 rgba(0, 0, 0, 0.19);

        }

        .square-padding {
            padding: 2px;
        }

        .sports_name {
            position: relative;
            top: 75px;
            color: yellow;
        }

        .hover {
            background-color: yellow;
            color: white;
        }

        .no-left-right-margin {
            margin-left: 0;
            margin-right: 0;
        }
    </style>
</head>
<body>

<div class="back-container"></div>

<div class="container">
<form action="{{action('UserController@addUserInfo')}}" method="POST" class="form-1">
        {{csrf_field()}}
        <div id="mycarousel" class="carousel slide" data-ride="carousel" data-interval="false">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <h1>What are your favorite sports?</h1>
                    <div class="row no-left-right-margin">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 square-padding">
                            <div class="favorite_sports_label">
                                <label class="sports_name" for="soccer">Soccer</label>
                                <input type="checkbox" style="display: none;" name="sports[]" value="Soccer">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 square-padding">
                            <div class="favorite_sports_label">
                                <label class="sports_name">Basketball</label>
                                <input type="checkbox" style="display: none;" name="sports[]" value="Basketball">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 square-padding">
                            <div class="favorite_sports_label">
                                <label class="sports_name">Tennis</label>
                                <input type="checkbox" style="display: none;" name="sports[]" value="Tennis">
                            </div>
                        </div>
                    </div>

                    <div class="row no-left-right-margin">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 square-padding">
                            <div class="favorite_sports_label">
                                <label class="sports_name">Football</label>
                                <input type="checkbox" style="display: none;" name="sports[]" value="Football">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 square-padding">
                            <div class="favorite_sports_label">
                                <label class="sports_name">Hockey</label>
                                <input type="checkbox" style="display: none;" name="sports[]" value="Hockey">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 square-padding">
                            <div class="favorite_sports_label">
                                <label class="sports_name">Baseball</label>
                                <input type="checkbox" style="display: none;" name="sports[]" value="Baseball">
                            </div>
                        </div>
                    </div>

                    <div class="row no-left-right-margin">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 square-padding">
                            <div class="favorite_sports_label">
                                <label class="sports_name">Rugby</label>
                                <input type="checkbox" style="display: none;" name="sports[]" value="Rugby">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 square-padding">
                            <div class="favorite_sports_label">
                                <label class="sports_name">Volleyball</label>
                                <input type="checkbox" style="display: none;" name="sports[]" value="Volleyball">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 square-padding">
                            <div class="favorite_sports_label">
                                <label class="sports_name">Lacrosse</label>
                                <input type="checkbox" style="display: none;" name="sports[]" value="Lacrosse">
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary right" href="#mycarousel" data-slide="next" style="margin-top: 20px;">Next</button>
                </div>
                <div class="item">
                    <h1>What is your zipcode?</h1>
                    <div class="input-group" style="width: 100%; margin-bottom: 10px;">
                        <input type="text" class="form-control" name="zipcode">
                    </div>
                    <button class="btn btn-warning left" href="#mycarousel" data-slide="prev">Previous</button>
                    <button class="btn btn-primary right" href="#mycarousel" data-slide="next">Skip</button>
                </div>
                <div class="item">
                    <h1>From 1 - 10, how would you rate your competitiveness?</h1>
                    <div class="input-group" style="width: 100%; margin-bottom: 10px;">
                        <input type="number" class="form-control" min="1" max="10" name="competitive_level">
                    </div>
                    <button class="btn btn-warning left" href="#mycarousel" data-slide="prev">Previous</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $(".favorite_sports_label").click(function () {
            $(this).toggleClass('hover');
            var attr = $(this).find('input').attr("checked");
            if (typeof attr !== typeof undefined && attr !== false) {
                $(this).find('input').attr("checked", false);
                $(this).children().css('color', 'yellow');
            }
            else {
                $(this).find('input').attr("checked", true);
                $(this).children().css('color', 'black');
            }
        });

    });

</script>
</body>
</html>