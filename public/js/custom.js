
$( document ).ready(function() {

    /*
        For previewing uploaded images
     */
    function readURL(input)
    {
        if (input.files && input.files[0])
        {
            let reader = new FileReader();
            reader.onload = function(e)
            {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#photo-upload").change(function()
    {
        readURL(this);
    });


    /*
        Getting all of the user info answers when a user first signs up.
     */
    $("#name, #email, #password, #password-confirm").change(function(){
        if($('#name').val() != '' && $('#email').val() != '' && $('#password').val() != '' && $('#password-confirm').val() != '')
        {
            $('.right').click();
        }
    });

    /*
        Deletes a photo from users photo records.
     */
    $("#delete-photo").click(function(){
        let photos = $(".mySlides");
        let photo_id = "";
        $(photos).each(function(i){
            if($(this).attr('style') != "display: none;")
            {
                photo_id = $(this).attr('id');
            }
        });
        $.ajax({
            type: "POST",
            url: '/delete_photo',
            data: {'photo_id': photo_id},
            success: "Success",
            dataType: "json",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function(){
            console.log("Done");
        });
    })


});


/*
    Nav bar Functions
 */
$(document).click(function (event) {
    let clickover = $(event.target);
    let $navbar = $(".navbar-collapse");
    let _opened = $navbar.hasClass("in");
    if (_opened === true && !clickover.hasClass("navbar-toggle")) {
        $navbar.collapse('hide');
    }
});

function openNav() {
    document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}




